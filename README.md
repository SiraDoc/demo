# SiraDoc Demo

[![pipeline status](https://gitlab.com/SiraDoc/demo/badges/master/pipeline.svg)](https://gitlab.com/SiraDoc/demo/-/commits/master)

This is a really simple demo website generated using SiraDoc. It can be viewed at https://SiraDoc.gitlab.io/demo/.
